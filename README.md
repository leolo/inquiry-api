# Inquiry API
### A small fastAPI (Python) to handle booking-requests of customers.

## Prerequisites:
- Git
- Docker
- Docker-Compose
## Using
- fastAPI
- redis (as database)
- webdavclient3 (handling nextcloud files)
- openpyxl (writing to an excel file)
- mjml (responsive email template framework)

### Automatic generated OpenAPI documentation is available here:
The API will be hosted per default at port 1234.

- {api-url}:1234/docs
### And alternative documentation here:
- {api-url}:1234/redocs

## Installation
`
git clone http://git.b-33.de/leo/b33backstage.git
`

## Development
Build the images:

`
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
`

And then start them:

`
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
`

## Stage
Build the images:

`
docker-compose -f docker-compose.yml -f docker-compose.stage.yml build
`

And then start them:

`
docker-compose -f docker-compose.yml -f docker-compose.stage.yml up -d
`

## Production

Build the images:

`
docker-compose -f docker-compose.yml build
`

And then start them:

`
docker-compose -f docker-compose.yml up -d
`

## Default User
Username: b33

Password: oink