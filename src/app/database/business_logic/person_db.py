from datetime import datetime, timezone
import json
from fastapi import HTTPException
from .business_logic import Business_Logic
import app.database.business_logic.inquiry_db as inquiry_db
import app.database.business_logic.archived_inquiry_db as archived_inquiry_db
from app.schema.person import Person, Person_Out_DB

class Person_DB(Business_Logic):
    def set_to_database(self, person: Person):
        """Saves or if already created updates object to the database.

        Returns if database has been changed.
        """
        person.id = self.create_id('person')
        person = self.check_if_person_already_exists(person)
        self.db_access.set_value(person.id, person.dict())
        return self.get_by_id(person.id)

    def update_to_database(self, person: Person):
        """Saves inquiry if existing record has been found.

        Returns if database has been changed.
        """
        if not self.check_if_exists(person.id):
            http_exception = HTTPException(
                status_code=404,
                detail=f'Could not find {person.id}.'
            )
            raise http_exception
        person = self.check_if_person_already_exists(person)
        self.db_access.update_value(person.id, person.dict())
        return self.get_by_id(person.id)

    def delete_from_database(self, person_id: str):
        """Deletes Person from the database if no associated inquiry is found.

        If person can not be found raises an error.
        """
        if not self.check_if_exists(person_id):
            http_exception = HTTPException(
                status_code=404,
                detail=f'Could not find {person_id}.'
            )
            raise http_exception

        inquiries_of_person = self.get_inquiries_of_person(person_id)
        number_of_inquiries = len(inquiries_of_person)

        archived_inquiries_of_person = self.get_archived_inquiries_of_person(person_id)
        number_of_archived_inquiries = len(archived_inquiries_of_person)

        if number_of_inquiries == 0 and number_of_archived_inquiries == 0:
            self.db_access.delete_key(person_id)

    def get_inquiries_of_person(self, person_id: str):
        inquirydb = inquiry_db.Inquiry_DB()
        inquiries = inquirydb.get_all_inquiries()

        person_inquiries = [inquiry for inquiry in inquiries
                            if person_id == inquiry['f_person_id']]

        return person_inquiries

    def get_archived_inquiries_of_person(self, person_id: str):
        archived_inquirydb = archived_inquiry_db.Archived_Inquiry_DB()
        archived_inquiries = archived_inquirydb.get_all_archived_inquiries()

        person_archived_inquiries = [archived_inquiry for archived_inquiry in archived_inquiries
                            if person_id == archived_inquiry['f_person_id']]

        return person_archived_inquiries

    def check_if_exists(self, person_id: str):
        """Checks if the person already exists and returns the result as boolean."""
        all_persons = self.get_all_persons()
        person_from_database = [person_db for person_db in all_persons
                                if person_db['id'] == person_id]
        if not person_from_database:
            return False
        else:
            return True

        
    def check_if_person_already_exists(self, person: Person):
        """Checks if person exists in database.
            
        Returns the Person with validation.
        """
        all_persons = self.get_all_persons()
        person_from_database = [person_db for person_db in all_persons
                                if person_db['email'] == person.email]

        if person_from_database:
            person_db = Person(**person_from_database[0])
            person_db.prename = person.prename
            person_db.name = person.name 

            person = person_db

        return person

    def get_all_persons_with_inquiries(self):
        """Gets all persons with their inquiries.

        Returns a list containing Inquiry objects nested in Person objects.
        """ 
        all_inquiries = []
        persons = self.get_all_persons()

        inquirydb = inquiry_db.Inquiry_DB()
        inquiries = inquirydb.get_all_inquiries()

        for inquiry in inquiries:
            person = [
                person for person in persons
                if person['id'] == inquiry['f_person_id']
            ]
            if person:
                all_inquiries.append({'inquiry': inquiry, 'person': person[0]})

        return all_inquiries

    def get_all_persons_with_archived_inquiries(self):
        """Gets all persons with their inquiries.

        Returns a list containing Inquiry objects nested in Person objects.
        """
        all_archived_inquiries = []
        persons = self.get_all_persons()
        archived_inquirydb = archived_inquiry_db.Archived_Inquiry_DB()
        archived_inquiries = archived_inquirydb.get_all_archived_inquiries()

        for archived_inquiry in archived_inquiries:
            person = [
                person for person in persons
                if person['id'] == archived_inquiry['f_person_id']
            ]
            all_archived_inquiries.append(
                {
                    'archived_inquiry': archived_inquiry,
                    'person': person[0]
                })

        return all_archived_inquiries

    def get_by_id(self, id: str):
        """Gets Person by id."""
        keys_of_all_persons = self.db_access.get_keys(
            match_string=id,
            all_keys=[]
            )

        person = Person_Out_DB(**self.db_access.get_values(keys_of_all_persons)[0])
        return person

    def get_all_persons(self):
        """Gets all persons stored in the database.

        The result will be returned as a list of Persons
        """
        keys_of_all_persons = self.db_access.get_keys(
            match_string='person:*',
            all_keys=[]
            )

        values_of_all_persons = self.db_access.get_values(keys_of_all_persons)
        return values_of_all_persons