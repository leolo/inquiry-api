import json
from fastapi import HTTPException
from .business_logic import Business_Logic
import app.database.business_logic.person_db as person_db
from app.accept_inquiry.update_excel import Update_Excel
from app.schema.inquiry import Inquiry_Out, Inquiry_To_Archive
from app.schema.inquiry_archive import Inquiry_Archive

class Archived_Inquiry_DB(Business_Logic):
    def set_to_database(self, inquiry: str, action: str):
        """Saves object to the database.

        Returns if database has been changed.
        """
        inquiry = Inquiry_To_Archive(**inquiry.dict())
        inquiry_archive_id = self.create_id('archived_inquiry')
        inquiry_archive = Inquiry_Archive(
            **inquiry.dict(),
            id=inquiry_archive_id,
            action=action
        )
        self.db_access.set_value(
            inquiry_archive_id,
            json.loads(inquiry_archive.json())
        )
        return inquiry_archive

    def delete_from_database(self, archived_inquiry_id: str):
        """Deletes Inquiry and Person if it has no Inquiry left, 
        
        from the database.
        """
        http_exception = HTTPException(
            status_code=400,
            detail=f'Can not delete archived inquiries with action = "accept".'
        )
        archived_inquiry = self.get_archived_inquiry_by_id(archived_inquiry_id)
        if archived_inquiry.action == 'accept':
            raise http_exception
        self.db_access.delete_key(archived_inquiry_id)
        persondb = person_db.Person_DB()
        persondb.delete_from_database(archived_inquiry.f_person_id)

    def get_archived_inquiry_by_id(self, archived_inquiry_id: str):
        """Gets an inquiry by id"""
        http_exception = HTTPException(
            status_code=404,
            detail=f'Could not find {archived_inquiry_id}.'
        )
        archived_inquiries_keys = self.db_access.get_keys(archived_inquiry_id, all_keys=[])
        if len(archived_inquiries_keys) == 0:
            raise http_exception
        archived_inquriy_values = self.db_access.get_values(archived_inquiries_keys)[0]
        archived_inquiry = Inquiry_Archive(**archived_inquriy_values)

        return archived_inquiry

    def get_all_archived_inquiries(self):
        """Gets all archived inquiries and returns them in a list"""
        archived_inquiries_keys = self.db_access.get_keys('archived_inquiry:*', all_keys=[])
        archived_inquiries = self.db_access.get_values(archived_inquiries_keys) 

        return archived_inquiries

    def recover_inquiry(self, archived_inquiry_id: str):
        archived_inquiry = self.get_archived_inquiry_by_id(archived_inquiry_id)
        archived_inquiry.id = archived_inquiry.f_inquriy_id
        if archived_inquiry.action == 'accept':
            self.remove_inquiry_from_excel(archived_inquiry.id)
        inquiry = Inquiry_Out(**archived_inquiry.dict())
        self.db_access.set_value(
            inquiry.id,
            json.loads(inquiry.json())
        )
        self.delete_from_database(archived_inquiry_id)

    def remove_inquiry_from_excel(self, inquiry_id: str):
        update_excel = Update_Excel()
        update_excel.handle_delete(inquiry_id)