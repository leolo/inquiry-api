from fastapi import HTTPException
from passlib.context import CryptContext
from .business_logic import Business_Logic
from ...schema.user import User, User_Post, User_Out, User_In_DB, User_From_DB

class User_DB(Business_Logic):
    pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')
    DEFAULT_USER = {'b33': {'username': 'b33', 'password': 'oink'}}

    def __init__(self, *args, **kwargs):
        Business_Logic.__init__(self)
        if not self.get_user_by_username(self.DEFAULT_USER['b33']['username']):
            self.set_to_db(User_Post(
                username=self.DEFAULT_USER['b33']['username'],
                password=self.DEFAULT_USER['b33']['password'],
                role='superuser'
            ))

    def get_users(self):
        """Gets all users and returns them in a list."""
        users_keys = self.db_access.get_keys('user*', all_keys=[])
        users_values = self.db_access.get_values(users_keys)
        users = [User_From_DB(**user_value) for user_value in users_values]
        return users

    def get_user_by_id(self, user_id: str):
        """Gets all users (should be max. one) that match the 
        
        user id and returns them in a list.
        """
        users = self.get_users()
        matched_user = [user for user in users if user.id == user_id]
        return matched_user

    def get_user_by_username(self, username: str):
        """Returns a user by username."""
        users = self.get_users()
        users_with_same_username = [
            user for user in users if user.username == username
        ]
        if users_with_same_username:
            return users_with_same_username[0]

    def set_to_db(self, user: User):
        """Saves a user to the db. 
        
        Raises an exception if username is already used.
        """
        if self.user_exists(user.username):
            raise HTTPException(
                status_code=400,
                detail=f'User with the name {user.username} already exists!'
            )
        hashed_password = self.hash_password(user.password)
        user = User_In_DB(**user.dict(), hashed_password=hashed_password)
        user.id = self.create_id('user')
        self.db_access.set_value(user.id, user.dict())
        return self.get_user_by_username(user.username)

    def update_to_db(self, user: User):
        """Updates an existing user. 

        If no existing user with a matching id exists raises an error.
        """ 
        if not self.get_user_by_id(user.id):
            raise HTTPException(
                status_code=404,
                detail=f'Could not find a user with the id: {user.id}'
            )
        hashed_password = self.hash_password(user.password)
        user = User_In_DB(**user.dict(), hashed_password=hashed_password)
        self.db_access.update_value(user.id, user.dict())
        return self.get_user_by_username(user.username)

    def delete_from_db(self, user_id: str):
        """Deletes an existing user. 

        If no existing user with a matching id exists raises an error.
        """ 
        if not self.get_user_by_id(user_id):
            raise HTTPException(
                status_code=404,
                detail=f'Could not find a user with the id: {user_id}'
            )
        self.db_access.delete_key(user_id)

    def user_exists(self, username: str):
        """Returns if a given username already exists."""
        users = self.get_users()
        users_with_same_username = [
            user for user in users if user.username == username
        ]
        if users_with_same_username:
            return True
        else:
            False

    def hash_password(self, password: str):
        return self.pwd_context.hash(password)

    def is_authorized(self, user_to_modify: User, user_logged_in: User):
        authentication_level = {
            'user': 0,
            'admin': 1,
            'superuser': 2
        }

        if authentication_level[user_logged_in.role] \
            > authentication_level[user_to_modify.role]:
            return True
        else:
            raise HTTPException(
                status_code=403,
                detail='You do not have permission to perform this action.'
            )