import uuid
import json
from datetime import datetime, timezone
from fastapi import HTTPException
from .business_logic import Business_Logic
import app.database.business_logic.person_db as person_db
import app.database.business_logic.archived_inquiry_db as archived_inquiry_db
from app.schema.inquiry import Inquiry_In, Inquiry_Out, Inquiry_To_Archive
from app.accept_inquiry.update_excel import Update_Excel

class Inquiry_DB(Business_Logic):
    def set_to_database(self, inquiry: Inquiry_In):
        """Saves or if already created updates object to the database.

        Returns if database has been changed.
        """
        inquiry.id = self.create_id('inquiry')
        self.db_access.set_value(inquiry.id, json.loads(inquiry.json()))
        return self.get_inquiry_by_id(inquiry.id)

    def update_to_database(self, inquiry: Inquiry_In):
        """Saves inquiry if existing record has been found.

        Returns if database has been changed.
        """
        # Check if inquiry already existed
        self.archive_inquiry(inquiry.id, 'update')
        self.db_access.update_value(inquiry.id, json.loads(inquiry.json()))
        return self.get_inquiry_by_id(inquiry.id)

    def delete_from_database(self, inquiry_id: str, action='delete'):
        """Deletes Inquiry and Person if it has no Inquiry left, 
        
        from the database.
        """
        inquiry = self.get_inquiry_by_id(inquiry_id)
        self.archive_inquiry(inquiry.id, action)
        self.db_access.delete_key(inquiry_id)

    def get_inquiry_by_id(self, inquiry_id: str):
        """Gets an inquiry by id"""
        http_exception = HTTPException(
            status_code=404,
            detail=f'Could not find {inquiry_id}.'
        )
        inquiries_keys = self.db_access.get_keys(inquiry_id, all_keys=[])
        if len(inquiries_keys) == 0:
            raise http_exception
        inquriy_values = self.db_access.get_values(inquiries_keys)[0]
        inquiry = Inquiry_Out(**inquriy_values)

        return inquiry

    def get_all_inquiries(self):
        """Gets all inquiries returns them in a list"""
        inquiries_keys = self.db_access.get_keys('inquiry:*', all_keys=[])
        inquiries = self.db_access.get_values(inquiries_keys) 

        return inquiries

    def archive_inquiry(self, inquiry_id: str, action: str):
        inquiry = self.get_inquiry_by_id(inquiry_id)
        archived_inquiry = archived_inquiry_db.Archived_Inquiry_DB()
        archived_inquiry.set_to_database(inquiry, action)

    def accept_inquiry(self, inquriy, person):
        update_excel = Update_Excel()
        update_excel.handle_update(inquriy, person)
        self.delete_from_database(inquriy.id, action='accept')