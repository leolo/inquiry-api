import uuid
from abc import ABC, abstractmethod
from ..database_layer.database import Database

class Business_Logic(ABC):
    def __init__(self, db_access: Database = Database()):
        self.db_access = Database()

    def create_id(self, id_prefix: str) -> str:
        return f'{id_prefix}:{uuid.uuid4()}'