import fakeredis

SERVER = fakeredis.FakeServer()
class Fake_Redis_Connection:
    def __init__(self):
        self.db = fakeredis.FakeStrictRedis(server=SERVER)