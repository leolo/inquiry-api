from datetime import datetime, timezone
from ..database_connection.redis import Redis_Connection

class Database:
    def __init__(self, db = Redis_Connection()):
        """Takes a db connection."""
        self.db = db.db

    def set_value(self, key, data):
        """Sets data to a redis storage.

        If resource already exists it will be over written,
        else created.
        """
        data.setdefault('created_at', datetime.now(timezone.utc).isoformat())
        data.setdefault('updated_at', datetime.now(timezone.utc).isoformat())
        return self.db.hmset(key, data)

    def update_value(self, key, data):
        """Updates data in a redis storage.

        If resource already exists it will be over written,
        else created.
        """
        data.setdefault('updated_at', datetime.now(timezone.utc).isoformat())
        return self.db.hmset(key, data)

    def get_keys(self, match_string=None, cursor=0, all_keys=[]):
        """Get all keys from the database that match the match_string.

        Returns a list of Key(s).
        """
        result = self.db.scan(cursor=cursor, match=match_string)
        new_cursor = result[0]
        all_keys += result[1]

        if new_cursor != 0:
            self.get_keys(match_string, cursor=new_cursor, all_keys=all_keys)

        return all_keys

    def get_values(self, keys):
        """Get values to the given keys.

        Returns a dictionary with the found values and the key.
        """
        values = []

        for key in keys:
            element = self.db.hgetall(key)
            element = self.convert_dic(element)
            values.append(element)

        return values

    def delete_key(self, key):
        """Deletes a key from the Database.

        Returns an integer with the number of deleted records.
        """
        return self.db.delete(key)

    def convert_dic(self, request_object):
        """Takes a dictionary containing of byte keys and values.

        Returns a dictionary with keys and values as strings.
        """
        new_dic = {key.decode("utf-8"):value.decode("utf-8") for key, value in request_object.items()}

        return new_dic