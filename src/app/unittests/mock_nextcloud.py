from app.accept_inquiry.web_dav import Web_DAV

class Mock_Update_Excel:
    def mock_init(self):
        file_server = 'Documents/test_b33_inquiry.xlsm'
        self.local_file = '/app/app/unittests/crud/test_files/test_b33_inquiry.xlsm'
        options = {
            'webdav_hostname': "http://nextcloud:80/remote.php/dav/files/b33",
            'webdav_login':    "b33",
            'webdav_password': "b33"
        }
        self.web_dav = Web_DAV(file_server, self.local_file, options)