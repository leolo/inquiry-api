import unittest
from unittest.mock import patch
import json
from starlette.testclient import TestClient
from app.main import app
from app.database.database_layer.database import Database
from app.database.business_logic.user_db import User_DB
from app.schema.user import User_Put, User_Out
from app.unittests.mock_database import Fake_Database

@patch.object(Database, '__init__', Fake_Database.mock_init)
class Test_User(unittest.TestCase):
    #patching a class doesnt include none test methods
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        self.endpoint = '/api/v1/users/me'
        self.client = TestClient(app)

        self.token = self.client.post(
            '/api/v1/token',
            data={'username': 'b33', 'password': 'oink'}
        )
        self.token = json.loads(self.token.text)

        self.test_user = self.client.post(
            '/api/v1/users/',
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={'username': 'test', 'password': 'test', 'role': 'user'}
        )
        self.person = json.loads(self.test_user.text)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def tearDown(self):
        try:
            self.test_user = self.client.delete(
                '/api/v1/users/' + self.person['id'],
                headers={'Authorization': f'Bearer {self.token["access_token"]}'}
            )
        except:
            pass

    def test_get_users(self):
        self.client = TestClient(app)
        response = self.client.get(
            '/api/v1/users/',
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)

    def test_get_me(self):
        self.client = TestClient(app)
        response = self.client.get(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)

    def test_create_user(self):
        self.client = TestClient(app)
        response = self.client.post(
            '/api/v1/users/',
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={'username': 'create_user', 'password': 'create_user', 'role': 'user'}
        )
        self.assertEqual(response.status_code, 201)

    def test_update_user(self):
        self.client = TestClient(app)
        response = self.client.put(
            '/api/v1/users/',
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={'id': self.person['id'], 'username': self.person['username'], 'password': 'test', 'role': 'user'}
        )
        self.assertEqual(response.status_code, 200)

    def test_update_me(self):
        self.client = TestClient(app)
        me = self.client.get(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        me = User_Out(**json.loads(me.text))
        response = self.client.put(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={
                'id': me.id,
                'username': 'bla',
                'password': 'tut',
                'role': me.role
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_delete_user(self):
        self.client = TestClient(app)
        response = self.client.delete(
            '/api/v1/users/' + self.person['id'],
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)

    def test_delete_me(self):
        self.client = TestClient(app)
        self.token = self.client.post(
            '/api/v1/token',
            data={'username': 'test', 'password': 'test'}
        )
        self.token = json.loads(self.token.text)

        response = self.client.delete(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)