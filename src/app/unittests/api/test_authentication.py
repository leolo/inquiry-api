import unittest
from starlette.testclient import TestClient
from app.main import app
from app.routers import authentication

class Test_Authentication(unittest.TestCase):
    def setUp(self):
        self.endpoint = '/api/v1/token'
        self.client = TestClient(app)

    def test_token(self):
        response = self.client.post(
            self.endpoint,
            data={'username': 'b33', 'password': 'oink'}
        )
        self.assertEqual(response.status_code, 200)

    def test_token_wrong_username(self):
        response = self.client.post(
            self.endpoint,
            data={'username': 'test', 'password': 'oink'}
        )
        self.assertEqual(response.status_code, 401)

    def test_token_wrong_password(self):
        response = self.client.post(
            self.endpoint,
            data={'username': 'b33', 'password': 'wrong_password'}
        )
        self.assertEqual(response.status_code, 401)