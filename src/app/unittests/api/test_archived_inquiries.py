import unittest
from unittest.mock import patch
import json
from starlette.testclient import TestClient
from app.main import app
from app.routers import inquiries
from app.database.database_layer.database import Database
from app.database.business_logic.inquiry_db import Inquiry_DB
from app.database.business_logic.person_db import Person_DB
from app.database.business_logic.archived_inquiry_db import Archived_Inquiry_DB
from app.schema.inquiry import Inquiry_Post, Inquiry_In
from app.schema.person import Person_Post, Person
from app.unittests.mock_database import Fake_Database

@patch.object(Database, '__init__', Fake_Database.mock_init)
class Test_Archived_Inquiries(unittest.TestCase):
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        self.endpoint = '/api/v1/archived_inquiries/'
        self.client = TestClient(app)
        self.token = self.client.post(
            '/api/v1/token',
            data={'username': 'b33', 'password': 'oink'}
        )
        self.token = json.loads(self.token.text)

        test_post_person = Person_Post(
            prename='Test Prename',
            name='Test Name',
            email='Test Email'
        )
        self.test_person = Person(**test_post_person.dict())
        person_db = Person_DB()
        self.test_person = person_db.set_to_database(self.test_person)
        test_post_inquiry = Inquiry_Post(
            arrival='2020-05-25',
            departure='2020-05-30',
            roomNorth=1,
            roomSouth=1,
            parkingLot=1,
            babyBed=1,
            numberAdults=2,
            numberReduced=1,
            message='Test Message'
        )
        self.test_inquiry = Inquiry_In(
            **test_post_inquiry.dict(),
            f_person_id=self.test_person.id
        )
        inquiry_db = Inquiry_DB()
        self.inquiry = inquiry_db.set_to_database(self.test_inquiry)
        inquiry_db.delete_from_database(self.inquiry.id)
    
    def test_get_archived_inquiries(self):
        response = self.client.get(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)

    def test_recover_inquiries(self):
        archived_inquiry = Archived_Inquiry_DB()
        archived_inquiry = archived_inquiry.get_all_archived_inquiries()
        archived_inquiry_id = archived_inquiry[0]['id']
        response = self.client.post(
            self.endpoint + archived_inquiry_id,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)
