import unittest
from unittest.mock import patch
import json
from starlette.testclient import TestClient
from app.main import app
from app.routers import inquiries
from app.database.database_layer.database import Database
from app.database.business_logic.inquiry_db import Inquiry_DB
from app.database.business_logic.person_db import Person_DB
from app.schema.inquiry import Inquiry_Post, Inquiry_In
from app.schema.person import Person_Post, Person
from app.unittests.mock_database import Fake_Database
from app.accept_inquiry.update_excel import Update_Excel
from app.unittests.mock_nextcloud import Mock_Update_Excel
from app.unittests.crud.test_update_excel import Test_Update_Excel

class Test_Inquiries(unittest.TestCase):
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        self.endpoint = '/api/v1/inquiries/'
        self.client = TestClient(app)
        self.token = self.client.post(
            '/api/v1/token',
            data={'username': 'b33', 'password': 'oink'}
        )
        self.token = json.loads(self.token.text)

        test_post_person = Person_Post(
            prename='Test Prename',
            name='Test Name',
            email='Test Email'
        )
        self.test_person = Person(**test_post_person.dict())
        person_db = Person_DB()
        self.test_person = person_db.set_to_database(self.test_person)
        test_post_inquiry = Inquiry_Post(
            arrival='2020-05-25',
            departure='2020-05-30',
            roomNorth=1,
            roomSouth=1,
            parkingLot=1,
            babyBed=1,
            numberAdults=2,
            numberReduced=1,
            message='Test Message'
        )
        self.test_inquiry = Inquiry_In(
            **test_post_inquiry.dict(),
            f_person_id=self.test_person.id
        )
        inquiry_db = Inquiry_DB()
        inquiry_db.set_to_database(self.test_inquiry)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_get_inquiries(self):
        self.client = TestClient(app)
        response = self.client.get(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_get_inquiries_unauthorized(self):
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 401)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_create_inquiry(self):
        response = self.client.post(
            self.endpoint,
            json={
                'inquiry': {
                    'arrival': '2020-05-25',
                    'departure': '2020-05-30',
                    'roomNorth': 1,
                    'roomSouth': 1,
                    'parkingLot': 1,
                    'babyBed': 1,
                    'numberAdults': 2,
                    'numberReduced' :1,
                    'message': 'Test Message'
                },
                'person': {
                    'prename': 'test prename',
                    'name': 'test name',
                    'email': 'test@email.com'
                }
            }
        )
        self.assertEqual(response.status_code, 201)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    @patch.object(Update_Excel, '__init__', Mock_Update_Excel.mock_init)
    def test_accept_inquiry(self):
        response = self.client.post(
            self.endpoint + self.test_inquiry.id,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={}
        )
        self.assertEqual(response.status_code, 201)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_update_inquiry(self):
        test_update_excel = Test_Update_Excel()
        test_update_excel.handle_example_file()
        test_post_person = Person_Post(
            prename='Update Prename',
            name='Update Name',
            email='Update Email'
        )
        test_person = Person(**test_post_person.dict())
        person_db = Person_DB()
        person_db.set_to_database(test_person)
        test_post_inquiry = Inquiry_Post(
            arrival='2020-05-25',
            departure='2020-05-30',
            roomNorth=1,
            roomSouth=1,
            parkingLot=1,
            babyBed=1,
            numberAdults=2,
            numberReduced=1,
            message='Message'
        )
        test_inquiry = Inquiry_In(
            **test_post_inquiry.dict(),
            f_person_id=test_person.id
        )
        inquiry_db = Inquiry_DB()
        inquiry_db.set_to_database(test_inquiry)

        response = self.client.put(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={
                'inquiry': {
                    'id': test_inquiry.id,
                    'f_person_id': test_person.id,
                    'arrival': '2020-07-05',
                    'departure': '2020-08-01',
                    'roomNorth': 1,
                    'roomSouth': 1,
                    'parkingLot': 1,
                    'babyBed': 1,
                    'numberAdults': 2,
                    'numberReduced' :1,
                    'message': 'updated message'
                },
                'person': {
                    'id': test_person.id,
                    'prename': 'updated prename',
                    'name': 'updated name',
                    'email': 'updated email'
                }
            }
        )
        self.assertEqual(response.status_code, 200)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_update_inquiry_unauthorized(self):
        response = self.client.put(
            self.endpoint,
            json={
                'inquiry': {
                    'id': 'fsf',
                    'f_person_id': 'afa',
                    'message': 'oi'
                },
                'person': {
                    'id': 'fasf',
                    'prename': 'hallo',
                    'name': 'test',
                    'email': 'post@test.de'
                }
            }
        )
        self.assertEqual(response.status_code, 401)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_update_inquiry_not_found(self):
        response = self.client.put(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={
                'inquiry': {
                    'id': 'not_myinquiry',
                    'f_person_id': self.test_person.id,
                    'arrival': '2020-05-25',
                    'departure': '2020-05-30',
                    'roomNorth': 1,
                    'roomSouth': 1,
                    'parkingLot': 1,
                    'babyBed': 1,
                    'numberAdults': 2,
                    'numberReduced' :1,
                    'message': 'Update Message'
                },
                'person': {
                    'id': self.test_person.id,
                    'prename': 'updated prename',
                    'name': 'updated name',
                    'email': 'updated email'
                }
            }
        )
        self.assertEqual(response.status_code, 404)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_update_inquiry_person_not_found(self):
        response = self.client.put(
            self.endpoint,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'},
            json={
                'inquiry': {
                    'id': self.test_inquiry.id,
                    'f_person_id': self.test_person.id,
                    'arrival': '2020-05-25',
                    'departure': '2020-05-30',
                    'roomNorth': 1,
                    'roomSouth': 1,
                    'parkingLot': 1,
                    'babyBed': 1,
                    'numberAdults': 2,
                    'numberReduced' :1,
                    'message': 'Update Message'
                },
                'person': {
                    'id': 'not_my_person',
                    'prename': 'updated prename',
                    'name': 'updated name',
                    'email': 'updated email'
                }
            }
        )
        self.assertEqual(response.status_code, 404)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_delete_inquiry(self):
        test_post_person = Person_Post(
            prename='Delete Prename',
            name='Delete Name',
            email='Delete Email'
        )
        test_person = Person(**test_post_person.dict())
        person_db = Person_DB()
        person_db.set_to_database(test_person)
        test_post_inquiry = Inquiry_Post(
            arrival='2020-05-25',
            departure='2020-05-30',
            roomNorth=0,
            roomSouth=0,
            parkingLot=0,
            babyBed=0,
            numberAdults=2,
            numberReduced=1,
            message='Test Delete'
        )
        test_inquiry = Inquiry_In(
            **test_post_inquiry.dict(),
            f_person_id=test_person.id
        )
        inquiry_db = Inquiry_DB()
        inquiry_db.set_to_database(test_inquiry)

        response = self.client.delete(
            self.endpoint + test_inquiry.id,
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 200)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_delete_inquiry_unauthorized(self):
        response = self.client.delete(
            self.endpoint + self.test_inquiry.id
        )
        self.assertEqual(response.status_code, 401)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_delete_inquiry_not_found(self):
        response = self.client.delete(
            '{self.endpoint}inquiry:not-my-inquiry',
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 404)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_delete_inquiry_not_valid(self):
        response = self.client.delete(
            f'{self.endpoint}not-valid-inquiry',
            headers={'Authorization': f'Bearer {self.token["access_token"]}'}
        )
        self.assertEqual(response.status_code, 422)