from app.database.database_connection.fakeredis import Fake_Redis_Connection

class Fake_Database:
    def mock_init(self):
        fake_redis = Fake_Redis_Connection()
        self.db = fake_redis.db