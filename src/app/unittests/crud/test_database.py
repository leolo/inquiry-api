import unittest
from app.database.database_connection.fakeredis import Fake_Redis_Connection
from app.database.database_layer.database import Database
from app.database.business_logic.person_db import Person

class Test_Database(unittest.TestCase):
    def setUp(self):
        self.mockup = Database(Fake_Redis_Connection())

        self.person = Person(prename='maresa', name='hagebutte', email='spam.account@gmail.de')

        self.key = 'test_hash'
        self.mockup.db.hmset(self.key, {
            'test_prop_1': 'test_value_1',
            'test_prop_2': 'test_value_2',
            'test_prop_3': 'test_value_3',
        })
    def test_get_mockup(self):
        self.assertTrue(self.mockup.db.ping())

    def test_set_value(self):
        self.mockup.set_value('my_key', {'test': 'my_value'})

        result = self.mockup.db.hscan('my_key')[1]
        self.assertEqual(result[b'test'], bytes('my_value', 'utf-8'))

    def test_get_keys(self):
        response = self.mockup.get_keys(self.key)
        self.assertEqual(response[0], bytes(self.key, "utf-8"))

    def test_get_values(self):
        response = self.mockup.get_keys(self.key)
        response = self.mockup.get_values(response)

        self.assertEqual(response[0]['test_prop_1'], 'test_value_1')
        self.assertEqual(response[0]['test_prop_2'], 'test_value_2')
        self.assertEqual(response[0]['test_prop_3'], 'test_value_3')

    def test_delete_key(self):
        self.mockup.delete_key(self.key)
        response = self.mockup.db.hscan(self.key)
        self.assertEqual(response[1], {})