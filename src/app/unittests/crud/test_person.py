import unittest
from unittest.mock import patch
from app.database.database_layer.database import Database
from app.database.business_logic.person_db import Person_DB
from app.schema.person import Person
from app.unittests.mock_database import Fake_Database

class Test_Person(unittest.TestCase):
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        self.person_db = Person_DB()
        
        request_form_valid_person = {
            'prename': 'magda',
            'name': 'musterfrau',
            'email': 'magda@musterfrau.de',
        }
        self.person = Person(**request_form_valid_person)
        self.person = self.person_db.set_to_database(self.person)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_set(self):
        person = {
            'prename': 'steven',
            'name': 'ulriksen',
            'email': 'steven@ulriksen.com',
        }
        person = Person(**person)
        person = self.person_db.set_to_database(person)
        self.assertEqual(
            self.person_db.db_access.get_keys(person.id, all_keys=[])[0],
            bytes(person.id, "utf-8"))

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_update(self):
        self.person.prename = 'Udo'
        self.person_db.update_to_database(self.person)
        result = self.person_db.db_access.get_values([self.person.id])
        self.assertEqual(result[0]['prename'], 'Udo')

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def test_delete(self):
        self.person_db.delete_from_database(self.person.id)
        self.assertEqual(
            self.person_db.db_access.get_keys(
                str(self.person.id), all_keys=[]), []
        )