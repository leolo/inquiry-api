import unittest
from unittest.mock import patch
from datetime import datetime
from app.database.business_logic.person_db import Person_DB
from app.database.business_logic.archived_inquiry_db import Archived_Inquiry_DB
import app.database.business_logic.inquiry_db as inquiry_db
from app.schema.inquiry import Inquiry_Out
from app.schema.person import Person
from app.database.database_layer.database import Database
from app.unittests.mock_database import Fake_Database

@patch.object(Database, '__init__', Fake_Database.mock_init)
class Test_Archived_Inquiries(unittest.TestCase):
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        person_data = {
            'prename': 'max',
            'name': 'mustermann',
            'email': 'max@mustermann.de'
        }
        self.person = Person(**person_data)
        person_db = Person_DB()
        self.person = person_db.set_to_database(self.person)
        inquiry = Inquiry_Out(
            id='inquiry:testblubb',
            f_person_id=self.person.id,
            arrival='2020-05-25',
            departure='2020-05-30',
            roomNorth=1,
            roomSouth=1,
            parkingLot=1,
            babyBed=1,
            numberAdults=2,
            numberReduced=1,
            message='Test Message',
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        self.archived_inquiry_db = Archived_Inquiry_DB()
        self.archived_inquiry = self.archived_inquiry_db.set_to_database(inquiry, 'update')

    def test_get(self):
        archived_inquiry = Archived_Inquiry_DB()
        archived_inquiries = archived_inquiry.get_all_archived_inquiries()
        self.assertGreaterEqual(len(archived_inquiries), 1)
    
    def test_delete_from_database(self):
        archived_inquiry = Archived_Inquiry_DB()
        archived_inquiry.delete_from_database(
            self.archived_inquiry.id
        )
        archived_inquiries = archived_inquiry.get_all_archived_inquiries()
        self.assertGreaterEqual(len(archived_inquiries), 0)
    
    def test_recover_inquiry(self):
        self.archived_inquiry_db.recover_inquiry(self.archived_inquiry.id)
        inquiry = inquiry_db.Inquiry_DB()
        with self.assertRaises(Exception):
            self.archived_inquiry_db.get_archived_inquiry_by_id(self.archived_inquiry.id)
        self.assertTrue(inquiry.get_inquiry_by_id(self.archived_inquiry.f_inquriy_id))