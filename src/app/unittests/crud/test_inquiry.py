import unittest
from unittest.mock import patch
from datetime import datetime
from app.schema.inquiry import Inquiry_Out
from app.schema.person import Person
from app.database.business_logic.inquiry_db import Inquiry_DB
from app.database.business_logic.person_db import Person_DB
from app.database.database_layer.database import Database
from app.unittests.mock_database import Fake_Database

@patch.object(Database, '__init__', Fake_Database.mock_init)
class Test_Inquiry(unittest.TestCase):
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        inquiry_data = {
            'arrival': '2020-07-05',
            'departure': '2020-08-01',
            'roomNorth': 1,
            'roomSouth': 1,
            'parkingLot': 1,
            'babyBed': 1,
            'numberAdults': 2,
            'numberReduced' :1,
            'message': 'Hello World',
            'created_at': datetime.now(),
            'updated_at': datetime.now()
        }
        self.inquiry = Inquiry_Out(**inquiry_data)
        person_data = {
            'prename': 'max',
            'name': 'mustermann',
            'email': 'max@mustermann.de'
        }
        self.person = Person(**person_data)
        person_db = Person_DB()
        self.person = person_db.set_to_database(self.person)
        self.inquiry.f_person_id = self.person.id
        inquiry_db = Inquiry_DB()
        self.inquiry = inquiry_db.set_to_database(self.inquiry)

    def test_set(self):
        inquiry_db = Inquiry_DB()
        inquiry_db.set_to_database(self.inquiry)
        
        self.assertEqual(str(inquiry_db.db_access.get_keys(match_string=self.inquiry.id, all_keys=[])[0], 'utf-8'),
                         str(self.inquiry.id))

    def test_update(self):
        inquiry_db = Inquiry_DB()
        inquiry_db.set_to_database(self.inquiry)
        self.inquiry.message = 'Ahoi!'
        inquiry_db.update_to_database(self.inquiry)
        result = inquiry_db.db_access.get_values([self.inquiry.id])
        result_archived = inquiry_db.db_access.get_keys(match_string="archived_inquiry:*")
        self.assertEqual(result[0]['message'], self.inquiry.message)
        self.assertGreaterEqual(len(result_archived), 1)
    
    def test_delete(self):
        inquiry_db = Inquiry_DB()
        inquiry_db.delete_from_database(self.inquiry.id)
        get_inquiry = inquiry_db.db_access.get_keys(match_string=self.inquiry.id, all_keys=[])
        result_archived = inquiry_db.db_access.get_keys(match_string="archived_inquiry:*")
        self.assertEqual([], [])
        self.assertGreaterEqual(len(result_archived), 1)