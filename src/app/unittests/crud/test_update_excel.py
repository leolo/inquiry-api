from datetime import datetime
import unittest
from unittest.mock import patch
from unittest.mock import MagicMock
from app.accept_inquiry.update_excel import Update_Excel
from app.accept_inquiry.web_dav import Web_DAV
from app.unittests.mock_nextcloud import Mock_Update_Excel

@patch.object(Update_Excel, '__init__', Mock_Update_Excel.mock_init)
class Test_Update_Excel(unittest.TestCase):
    def setUp(self):
        self.handle_example_file()

    def handle_example_file(self):
        file_server = 'Documents/test_b33_inquiry.xlsm'
        file_local = '/app/app/unittests/crud/test_files/test_b33_inquiry.xlsm'
        options = {
            'webdav_hostname': "http://nextcloud:80/remote.php/dav/files/b33",
            'webdav_login':    "b33",
            'webdav_password': "b33"
        }
        web_dav = Web_DAV(file_server, file_local, options)
        try:
            web_dav.get_file()
        except:
            web_dav.client.upload('Documents/test_b33_inquiry.xlsm', '/app/app/unittests/crud/test_files/test_b33_inquiry.xlsm')

    def test_update_excel(self):
        inquiry = MagicMock()
        inquiry.id = 'test id'
        inquiry.created_at = datetime.now()
        inquiry.arrival = '2020-06-06'
        inquiry.departure = '2020-06-09'
        inquiry.roomNorth = 1
        inquiry.roomSouth = 0
        inquiry.parkingLot = 1
        inquiry.babyBed = 1
        inquiry.numberAdults = 1
        inquiry.numberReduced = 1
        inquiry.message = 'test'

        person = MagicMock()
        person.prename = 'test prename'
        person.name = 'test name'
        person.email = 'test@mail.de'

        update_excel = Update_Excel()
        self.assertIsNone(update_excel.handle_update(inquiry, person))

    def test_delete_excel(self):
        update_excel = Update_Excel()
        self.assertIsNone(update_excel.handle_delete('test id'))