import unittest
from unittest.mock import patch
from app.database.business_logic.user_db import User_DB
from app.schema.user import User, User_Post
from app.database.database_layer.database import Database
from app.unittests.mock_database import Fake_Database

class Test_User(unittest.TestCase):
    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def setUp(self):
        self.user_db = User_DB()
        self.user = User(
            username='max',
            password='meyer',
            role='user'
        )
        self.user = User_Post(**self.user.dict())
        self.user = self.user_db.set_to_db(self.user)

    @patch.object(Database, '__init__', Fake_Database.mock_init)
    def tearDown(self):
        if self.user_db.get_user_by_id(self.user.id):
            self.user_db.delete_from_db(self.user.id)

    def test_get_users(self):
        created_user = self.user_db.get_user_by_id(self.user.id)[0]
        self.assertEqual(self.user.id, created_user.id)

    def test_set_to_db(self):
        user = User(
            username='test_username',
            password='test_password',
            role='user'
        )
        user = User_Post(**user.dict())
        user = self.user_db.set_to_db(user)
        created_user = self.user_db.get_user_by_id(user.id)[0]

        self.assertEqual(user.id, created_user.id)
        self.assertEqual(user.username, created_user.username)

    def test_delete_from_db(self):
        self.user_db.delete_from_db(self.user.id)
        deleted_user = self.user_db.get_user_by_id(self.user.id)
        self.assertEqual(deleted_user, [])
