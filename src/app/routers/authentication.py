from datetime import datetime, timedelta
import jwt
from jwt import PyJWTError
from fastapi import APIRouter, Depends, Security, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext
from starlette.status import HTTP_401_UNAUTHORIZED
from app.schema.user import User, Token, TokenData
from app.database.business_logic.user_db import User_DB

router = APIRouter()

pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto') 

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='api/v1/token')

SECRET_KEY = '2d0c9062b87b7139abee086b0846d96b847472303a0bdcb83c2b7fc43804b676'
ALGORITHM = 'HS256'
ACCESS_TOKEN_EXPIRE_MINUTES = 90

def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get('sub')
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except PyJWTError:
        raise credentials_exception
    user = get_user(token_data.username)
    if user is None:
        raise credentials_exception
    return user

def get_current_active_admin(current_user: User = Security(get_current_user)):
    if current_user.role == 'user':
        raise HTTPException(
            status_code=403, detail="The user doesn't have enough privileges"
        )
    return current_user

def authenticate_user(username: str, password: str):
    user = get_user(username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user

def get_user(username):
    user_db = User_DB()
    return user_db.get_user_by_username(username)

def verify_password(plain_password: str, hash_password: str):
    return pwd_context.verify(plain_password, hash_password)

def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({'exp': expire})
    encode_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encode_jwt

@router.post('/api/v1/token')
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'}
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={'sub': user.username}, expires_delta=access_token_expires
    )
    return {
        'access_token': access_token, 
        'token_type': 'bearer'
    }