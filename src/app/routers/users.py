from typing import List
from fastapi import APIRouter, Depends, Path, HTTPException
from starlette.status import HTTP_401_UNAUTHORIZED
from app.database.business_logic.user_db import User_DB
from app.schema.user import User, User_Out, User_Post, User_Put, User_Put_Me
from app.routers.authentication import get_current_user, get_current_active_admin

router = APIRouter()

@router.get('/', status_code=200, response_model=List[User_Out])
def get_users(user: User = Depends(get_current_active_admin)):
    user_db = User_DB()
    return user_db.get_users()

@router.get('/me', status_code=200, response_model=User_Out)
def get_me(user: User = Depends(get_current_user)):
    return user

@router.post('/', status_code=201, response_model=User_Out)
def create_user(new_user: User, logged_in_user: User = Depends(get_current_active_admin)):
    user_db = User_DB()
    if user_db.is_authorized(new_user, logged_in_user):
        return user_db.set_to_db(new_user)

@router.put('/', status_code=200, response_model=User_Out)
def update_user(update_user: User_Put, logged_in_user: User = Depends(get_current_active_admin)):
    user_db = User_DB()
    if user_db.is_authorized(update_user, logged_in_user):
        return user_db.update_to_db(update_user)

@router.put('/me', status_code=200, response_model=User_Out)
def update_me(update_user: User_Put_Me, logged_in_user: User = Depends(get_current_user)):
    user_db = User_DB()
    if update_user.id == logged_in_user.id:
        user = user_db.get_user_by_id(update_user.id)[0]
        update_user = User_Put(**update_user.dict(), role=user.role)
        return user_db.update_to_db(update_user)
    else: 
        raise HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Not allowed to modify other user!"
    )

@router.delete('/me', status_code=200)
def delete_me(user: User = Depends(get_current_user)):
    user_db = User_DB()
    user_db.delete_from_db(user.id)
    
@router.delete('/{user_id}', status_code=200)
def delete_user(user_id: str = Path(None, regex='^user:'), user: User = Depends(get_current_active_admin)):
    user_db = User_DB()
    delete_user = user_db.get_user_by_id(user_id)[0]
    if user_db.is_authorized(delete_user, user):
        user_db.delete_from_db(user_id)