from fastapi import APIRouter, Depends
from ..database.business_logic.archived_inquiry_db import Archived_Inquiry_DB
from ..database.business_logic.person_db import Person_DB
from ..schema.user import User
from .authentication import get_current_user

router = APIRouter()

@router.get('/', status_code=200)
def get_archived_inquiries(user: User = Depends(get_current_user)):
    person_db = Person_DB()
    return person_db.get_all_persons_with_archived_inquiries()

@router.post('/{archived_inquiry_id}', status_code=200)
def recover_inquiry(archived_inquiry_id: str, user: User = Depends(get_current_user)):
    archived_inquiry_db = Archived_Inquiry_DB()
    return archived_inquiry_db.recover_inquiry(archived_inquiry_id)

@router.delete('/{archived_inquiry_id}', status_code=200)
def delete_archived_inquiries(archived_inquiry_id: str, user: User = Depends(get_current_user)):
    archived_inquiry_db = Archived_Inquiry_DB()
    return archived_inquiry_db.delete_from_database(archived_inquiry_id)