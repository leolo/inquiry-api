import asyncio
from fastapi import APIRouter, Depends, Path
from ..database.business_logic.person_db import Person_DB
from ..database.business_logic.inquiry_db import Inquiry_DB
from ..schema.inquiry import Inquiry_In, Inquiry_Post, Inquiry_Put, Inquiry_Return
from ..schema.person import Person, Person_Post, Person_Put
from ..schema.user import User
from .authentication import get_current_user
import app.email.notification_mail as notification_mail

router = APIRouter()

@router.get('/', status_code=200)
def get_inquiries(user: User = Depends(get_current_user)):
    person_db = Person_DB()
    return person_db.get_all_persons_with_inquiries()
    
@router.post('/', status_code=201, response_model=Inquiry_Return)
def create_inquiry(inquiry: Inquiry_Post, person: Person_Post):
    person = Person(**person.dict())
    person_db = Person_DB()
    person = person_db.set_to_database(person)

    inquiry = Inquiry_In(**inquiry.dict(), f_person_id=person.id)
    inquiry_db = Inquiry_DB()
    inquiry = inquiry_db.set_to_database(inquiry)

    asyncio.run(notification_mail.send_notification_email(inquiry, person))
    return Inquiry_Return(person=person, inquiry=inquiry)

@router.post('/{inquiry_id}', status_code=201)
def accept_inquiry(inquiry_id: str = Path(None, regex='^inquiry:'),
        user: User = Depends(get_current_user)):
    """Uploads inquiry information to remote excel file and moves the 
    inquiry to the archived inquiries.
    """
    inquiry_db = Inquiry_DB()
    inquiry = inquiry_db.get_inquiry_by_id(inquiry_id)
    person_db = Person_DB()
    person = person_db.get_by_id(inquiry.f_person_id)
    inquiry_db.accept_inquiry(inquiry, person)

@router.put('/', status_code=200, response_model=Inquiry_Return)
def update_inquiry(inquiry: Inquiry_Put, person: Person_Put,
        user: User = Depends(get_current_user)):
    person_db = Person_DB()
    person = person_db.update_to_database(Person(**person.dict()))
    inquiry_db = Inquiry_DB()
    inquiry = inquiry_db.update_to_database(Inquiry_In(**inquiry.dict()))
    return Inquiry_Return(person=person, inquiry=inquiry)

@router.delete('/{inquiry_id}', status_code=200)
def delete_inquiry(inquiry_id: str = Path(None, regex='^inquiry:'), user: User = Depends(get_current_user)):
    inquiry_db = Inquiry_DB()
    inquiry_db.delete_from_database(inquiry_id)