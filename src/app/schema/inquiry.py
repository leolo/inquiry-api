import uuid
from datetime import datetime
from pydantic import BaseModel, Field
from app.schema.person import Person_Out_DB

class Inquiry_In(BaseModel):
    id: str = f'inquiry:{uuid.uuid4()}'
    f_person_id: str = None
    arrival: str
    departure: str
    roomNorth: int
    roomSouth: int
    parkingLot: int
    babyBed: int
    numberAdults: int
    numberReduced: int
    message: str

class Inquiry_Out(BaseModel):
    id: str = f'inquiry:{uuid.uuid4()}'
    f_person_id: str = None
    arrival: str
    departure: str
    roomNorth: int
    roomSouth: int
    parkingLot: int
    babyBed: int
    numberAdults: int
    numberReduced: int
    message: str
    created_at: datetime
    updated_at: datetime

class Inquiry_Post(BaseModel):
    arrival: str
    departure: str
    roomNorth: int
    roomSouth: int
    parkingLot: int
    babyBed: int
    numberAdults: int
    numberReduced: int
    message: str

class Inquiry_Put(BaseModel):
    id: str
    f_person_id: str
    arrival: str
    departure: str
    roomNorth: int
    roomSouth: int
    parkingLot: int
    babyBed: int
    numberAdults: int
    numberReduced: int
    message: str

class Inquiry_Return(BaseModel):
    inquiry: Inquiry_Out
    person: Person_Out_DB

class Inquiry_To_Archive(BaseModel):
    f_inquriy_id: str = Field(None, alias='id')
    f_person_id: str = None
    arrival: str
    departure: str
    roomNorth: int
    roomSouth: int
    parkingLot: int
    babyBed: int
    numberAdults: int
    numberReduced: int
    message: str
    created_at: datetime
    updated_at: datetime