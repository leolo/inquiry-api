from datetime import datetime
from pydantic import BaseModel

class Inquiry_Archive(BaseModel):
    id: str
    f_inquriy_id: str
    f_person_id: str = None
    arrival: str
    departure: str
    roomNorth: int
    roomSouth: int
    parkingLot: int
    babyBed: int
    numberAdults: int
    numberReduced: int
    message: str
    action: str
    created_at: datetime
    updated_at: datetime