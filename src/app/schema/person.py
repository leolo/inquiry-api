import uuid
from datetime import datetime
from pydantic import BaseModel

class Person(BaseModel):
    id: str = f'person:{uuid.uuid4()}'
    prename: str
    name: str
    email: str

class Person_Post(BaseModel):
    prename: str
    name: str
    email: str

class Person_Put(BaseModel):
    id: str
    prename: str
    name: str
    email: str

class Person_Out_DB(BaseModel):
    id: str = f'person:{uuid.uuid4()}'
    prename: str
    name: str
    email: str
    created_at: datetime
    updated_at: datetime