import uuid
from datetime import datetime
from pydantic import BaseModel, Field

class User(BaseModel):
    username: str
    password: str
    role: str = Field(..., regex='user|admin|superuser')

class User_Post(BaseModel):
    id: str = f'user:{uuid.uuid4()}'
    username: str
    password: str
    role: str = Field(..., regex='user|admin|superuser')

class User_Put(BaseModel):   
    id: str
    username: str
    password: str
    role: str = Field(..., regex='user|admin|superuser')

class User_Put_Me(BaseModel):   
    id: str
    username: str
    password: str

class User_In_DB(BaseModel):
    id: str = f'user:{uuid.uuid4()}'
    username: str
    hashed_password: str   
    role: str = Field(..., regex='user|admin|superuser')

class User_From_DB(BaseModel):
    id: str = f'user:{uuid.uuid4()}'
    username: str
    hashed_password: str   
    created_at: datetime
    updated_at: datetime
    role: str = Field(..., regex='user|admin|superuser')

class User_Out(BaseModel):
    id: str
    username: str
    role: str = Field(..., regex='user|admin|superuser')
    created_at: datetime
    updated_at: datetime

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: str = None