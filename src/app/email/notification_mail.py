import os
import smtplib
import logging
from datetime import date
import requests
from email.message import EmailMessage
from app.schema.inquiry import Inquiry_Out
from app.schema.person import Person

async def send_notification_email(inquiry: Inquiry_Out, person: Person):
    try:
        smtp_server = os.getenv('SMTP_SERVER_URL')
        smtp_username = os.getenv('SMTP_USERNAME')
        smtp_password = os.getenv('SMTP_PASSWORD')
        email_sender = os.getenv('EMAIL_SENDER')
        email_recipient = os.getenv('EMAIL_RECIPIENT')

        email = create_email(inquiry, person, email_sender, email_recipient)

        with smtplib.SMTP(smtp_server) as smtp:
            smtp.login(smtp_username, smtp_password)
            smtp.send_message(email)
    except:
        logging.error(f'Failed to send notification email for inquiry {inquiry.id}')

def create_email(inquiry: Inquiry_Out, person: Person,
        email_sender: str, email_recipient: str):

    email_message = fill_email_template(inquiry, person)
    email_message_encoded = encode(email_message)

    # render responsive mjml email
    response = requests.post(
        'http://mjml',
        headers={'Content-Type': 'text/plain'},
        data=email_message_encoded
    )

    email = EmailMessage()
    email.set_content(response.text, subtype='html')

    email['Subject'] = get_email_subject(inquiry, person)
    email['From'] = email_sender
    email['To'] = email_recipient

    return email

def fill_email_template(inquiry: Inquiry_Out, person: Person):
    with open('app/email/email_template.mjml') as email_message:
        email_message = email_message.read()

    frontend_url = f'{os.getenv("FRONTEND_URL")}/inquiries?filter={inquiry.id}'
    email_message = email_message.format(
        inquiry_id=inquiry.id,
        name=person.name,
        prename=person.prename,
        email=person.email,
        frontend_url=frontend_url,
        arrival=get_formatted_date(inquiry.arrival),
        departure=get_formatted_date(inquiry.departure),
        rooms=get_rooms(inquiry),
        extras=get_extras(inquiry),
        number_adults=inquiry.numberAdults,
        number_reduced=inquiry.numberReduced,
        message=inquiry.message
    )

    return email_message

def get_formatted_date(date_to_format: str):
    date_to_format = date.fromisoformat(date_to_format)
    return date_to_format.strftime('%d.%m.%Y')

def get_rooms(inquiry: Inquiry_Out):
    rooms = ''

    if inquiry.roomNorth:
        rooms = 'Nord '
    if inquiry.roomSouth:
        rooms += 'Süd'

    return rooms

def get_extras(inquiry: Inquiry_Out):
    extras = ''

    if inquiry.parkingLot:
        extras = 'Stellplatz '
    if inquiry.babyBed:
        extras += 'Babybett'

    return extras

def encode(text: str) -> str:
    text = text.replace(r'\n', '')
    return text.encode('ascii', 'xmlcharrefreplace')

def get_email_subject(inquiry: Inquiry_Out, person: Person):
    return f'Neue Anfrage: {person.name}, {person.prename} ' \
    + f'| {get_formatted_date(inquiry.arrival)} - {get_formatted_date(inquiry.departure)} ' \
    + f'| {get_rooms(inquiry)}'