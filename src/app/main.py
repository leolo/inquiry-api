import os
import uvicorn
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.routers import authentication, inquiries, users, archived_inquiries

app = FastAPI(
    title="Voelkerfreunde API"
)

origins = [
    os.getenv('BACKEND_CORS_ORIGINS')
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

app.include_router(
    authentication.router,
    tags=['Authentication']
)
app.include_router(
    inquiries.router,
    prefix='/api/v1/inquiries',
    tags=['Inquiries']
)
app.include_router(
    archived_inquiries.router,
    prefix='/api/v1/archived_inquiries',
    tags=['Archived Inquiries']
)
app.include_router(
    users.router,
    prefix='/api/v1/users',
    tags=['Users']
)