from pathlib import Path
from webdav3.client import Client

class Web_DAV:
    def __init__(self, file_server: str, file_local: str, options: dict):
        self.file_server = file_server
        self.file_local = file_local
        self.info = ''

        self.client = self.set_up_client(options)

    def set_up_client(self, options):
        return Client(options)

    def get_file(self):
        self.set_up_local_file()
        resource =  self.client.resource(self.file_server)
        self.info = resource.info()
        resource.write(self.file_local)
    
    def set_up_local_file(self):
        local_file = Path(self.file_local)
        if not local_file.is_file():
            local_file.touch()

    def upload_file(self):
        resource = self.client.resource(self.file_server)
        resource.read(self.file_local)
        self.tear_down_local_file()
    
    def tear_down_local_file(self):
        local_file = Path(self.file_local)
        if local_file.is_file():
            local_file.unlink()       

