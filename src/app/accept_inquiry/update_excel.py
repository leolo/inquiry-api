from datetime import datetime, date
from copy import copy
import openpyxl
from openpyxl.formula.translate import Translator
from app.accept_inquiry.web_dav import Web_DAV

WORK_SHEET_NAME = 'Buchungen'

class Update_Excel:
    def __init__(self):
        file_server = 'Documents/AKTUELL_b33_Buchungen_Gaestezimmer_BHSt.xlsm'
        self.local_file = './AKTUELL_b33_Buchungen_Gaestezimmer_BHSt.xlsm'
        options = {
            'webdav_hostname': "http://nextcloud:80/remote.php/dav/files/b33",
            'webdav_login':    "b33",
            'webdav_password': "b33"
        }
        self.web_dav = Web_DAV(file_server, self.local_file, options)

    def handle_update(self, inquiry, person):
        self.web_dav.get_file()
        self.write(inquiry, person)
        self.check_if_content_up_to_date(
            self.handle_update,
            inquiry,
            person
        )

    def write(self, inquiry, person):
        wb = openpyxl.load_workbook(self.local_file)
        ws = wb[WORK_SHEET_NAME]

        self.add_booking_row(ws, inquiry, person)
        self.add_booking_style(ws)
        self.handle_export_worksheet(wb, ws)

        wb.save(self.local_file)

    def add_booking_row(self, worksheet, inquiry, person):
        worksheet.append(
            [
                inquiry.id,
                date.fromisoformat(inquiry.created_at.strftime('%Y-%m-%d')),
                date.fromisoformat(inquiry.arrival),
                date.fromisoformat(inquiry.departure),
                self.format_room(inquiry.roomSouth),
                self.format_room(inquiry.roomNorth),
                f'{person.prename} {person.name}',
                self.get_extra(inquiry),
                '',
                '',
                inquiry.numberAdults,
                inquiry.numberReduced,
                self.get_nights(inquiry),
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '',
                person.email
            ]
        )

    def format_room(self, room_value):
        room = ''

        if room_value:
            room = 'x'
        else: room = ''

        return room

    def get_extra(self, inquiry):
        extras = ''

        if inquiry.parkingLot:
            extras = 'Stellplatz! '
        
        if inquiry.babyBed:
            extras += 'Babybettchen'

        return extras

    def get_nights(self, inquiry):
        arrival = date.fromisoformat(inquiry.arrival)
        departure = date.fromisoformat(inquiry.departure)

        nights = departure - arrival

        return nights.days

    def add_booking_style(self, worksheet):
        self.copy_style(worksheet, worksheet.max_row, 100)
        self.copy_range(worksheet, worksheet.max_row, 'D100:D100')
        self.copy_range(worksheet, worksheet.max_row, 'N100:AC100')
        self.copy_range(worksheet, worksheet.max_row, 'AF100:AF100')
        worksheet._tables[0].ref = f'A10:AS{worksheet.max_row}'

    def copy_style(self, worksheet, dest_row: int, source_row: int):
        source_row = worksheet[source_row]

        for cell in source_row:
            dest_cell = f'{cell.column_letter}{dest_row}'
            self.add_booking_data_validation(worksheet, dest_cell, cell)
            worksheet[dest_cell]._style = cell._style

    def add_booking_data_validation(self, worksheet, dest_cell: str, source_cell):
        for data_validation in worksheet.data_validations.dataValidation:
            if source_cell in data_validation:
                data_validation.add(dest_cell)

    def copy_range(self, worksheet, dest_row: int, source_range: str):
        copy_range = worksheet[source_range]

        for row in copy_range:
            for cell in row:
                dest_cell = f'{cell.column_letter}{dest_row}'
                worksheet[dest_cell].value = cell.value
    
    def handle_export_worksheet(self, workbook, buchungen_worksheet):
        export_worksheet_name = 'export'
        export_worksheet = workbook[export_worksheet_name]

        if buchungen_worksheet.max_row >= export_worksheet.max_row:   
            self.add_export_row(export_worksheet)

    def add_export_row(self, export_worksheet):
        export_worksheet.append([''])
        current_row = export_worksheet.max_row
        previous_row_range = export_worksheet[current_row - 1]

        self.copy_style(export_worksheet, current_row, current_row - 1)

        for cell in previous_row_range:
            destination_cell = f'{cell.column_letter}{current_row}'
            export_worksheet[destination_cell] = Translator(
                cell.value,
                cell.coordinate
            ).translate_formula(destination_cell)
                
    def handle_delete(self, inquiry_id: str):
        self.web_dav.get_file()
        self.delete(inquiry_id)
        self.check_if_content_up_to_date(
            self.handle_delete,
            inquiry_id
        )            
    
    def delete(self, inquiry_id: str):
        wb = openpyxl.load_workbook(self.local_file)
        ws = wb[WORK_SHEET_NAME]

        for row in ws.iter_rows(min_row=1, max_col=1):
            for cell in row:
                if cell.value == inquiry_id:
                    ws.delete_rows(cell.row)

        ws._tables[0].ref = f'A10:AS{ws.max_row}'
        wb.save(self.local_file)
    
    def check_if_content_up_to_date(self, func, *args):
        resource = self.web_dav.client.resource(self.web_dav.file_server)
        last_modified = resource.info()['modified']
        
        if self.web_dav.info['modified'] == last_modified:
            self.web_dav.upload_file()
        else:
            func(*args)